#ifndef PROJECT_IMAGE_STRUCTURES_H
#define PROJECT_IMAGE_STRUCTURES_H

#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void free_image(struct image* img);

#endif //PROJECT_IMAGE_STRUCTURES_H
