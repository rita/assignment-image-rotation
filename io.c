

#include <stdbool.h>
#include <stdint.h>
#include <malloc.h>
#include "io.h"
#include "bmp.h"
#include "util.h"


void read_image_from_file(const char* name, struct image* img ) {
    FILE* in = fopen(name, "rb");
    enum read_status result = from_bmp(in, img);
    fclose(in);
    if(result) err("File read incorreclty, error no %d\n", result);
}


void write_image_to_file(const char* name, struct image const* img ){
    FILE* out = fopen(name, "wb");
    enum write_status result = to_bmp(out, img);
    fclose(out);
    if(result) err("File written incorreclty, error no %d\n", result);

}
