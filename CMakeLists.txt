cmake_minimum_required(VERSION 3.17)
project(Project)

set(CMAKE_CXX_STANDARD 14)

add_executable(Project main.c util.h util.c bmp.c bmp.h image_structures.h image_manipulation.c image_manipulation.h io.c io.h image_structures.c)