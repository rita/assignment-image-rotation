#ifndef _BMP_H_
#define _BMP_H_


#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "io.h"

enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );

#endif
