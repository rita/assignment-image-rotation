#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

#include "bmp.h"
#include "util.h"
#include "image_structures.h"
#include "image_manipulation.h"


void usage(){
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n" );
    //if (argc > 3) err("Too many arguments \n" );
    struct image img = {0};
    read_image_from_file(argv[1], &img);

    struct image rotated = rotate(img);
    write_image_to_file(argv[2], &rotated);
    free_image(&img);

//    write_image_to_file(argv[2], &img);

//    struct bmp_header h = { 0 };
//    if (read_header_from_file( argv[1], &h )) {
//        bmp_header_print( &h, stdout );
//    }
//    else {
//        err( "Failed to open BMP file or reading header.\n" );
//    }

    return 0;
}
