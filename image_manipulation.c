

#include <malloc.h>
#include "image_manipulation.h"
#include "bmp.h"


struct image rotate( struct image const source ){
    struct image result = {
            .width = source.height,
            .height = source.width,
            .data = malloc(source.width * source.height * sizeof(struct pixel))
    };

    for (size_t i = 0; i < result.height; i++) {
        for (size_t j = 0; j < result.width; j++) {
            result.data[i * source.height + j] = source.data[source.width * (source.height - j - 1) + i];
        }
    }

    return result;
}