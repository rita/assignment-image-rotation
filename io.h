#ifndef PROJECT_IO_H
#define PROJECT_IO_H

#include <stdio.h>
#include "image_structures.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_EOF
    /* коды других ошибок  */
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

void read_image_from_file(const char* name, struct image* img );
void write_image_to_file(const char* name, struct image const* img );

#endif //PROJECT_IO_H
