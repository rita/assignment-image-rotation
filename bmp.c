#include "bmp.h"
#include "image_structures.h"
#include "util.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)



static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static uint8_t getPadding(size_t width){
    return (4 - width * 3 % 4) % 4;
}
static bool readPadding(FILE* in, size_t width){
    if(!getPadding(width)) return true;

    if(fseek(in, getPadding(width), SEEK_CUR) == 0) return true;
    return false;
//    int8_t *padding[4];
//    if (fread(padding, sizeof(uint8_t), getPadding(width), in) == 0)
//        return false;
//    return true;
}

enum read_status from_bmp( FILE* in, struct image* img ) {

    struct bmp_header header = (struct bmp_header) {0};
    if(!read_header(in, &header)) return READ_INVALID_HEADER;

    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = malloc(sizeof(struct pixel) * header.biWidth * header.biHeight);


    for (size_t i = 0; i < header.biHeight; i++) {
        if (fread(img->data + i * header.biWidth,
                    sizeof(struct pixel), header.biWidth, in) == 0)
            return READ_EOF;
        //if(getPadding(header.biWidth)) continue;
        if(!readPadding(in, header.biWidth)) return READ_EOF;
    }
    return READ_OK;
}


enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = (struct bmp_header) {
            .bfType = 19778, //Код bmp файла
            .bfileSize = img->width*img->height*sizeof(struct pixel)*8+sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = sizeof(struct pixel)*8,
            .biCompression = 0,
            .biSizeImage = sizeof(struct bmp_header) + img->width*img->height*sizeof(struct pixel)*8,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    fwrite(&header, sizeof (struct bmp_header), 1, out);
    uint8_t padding = 0;
    for(size_t i = 0; i < header.biHeight; i++) {
        fwrite(img->data+i*header.biWidth, sizeof(struct pixel), header.biWidth, out);
        fwrite(&padding, sizeof (uint8_t), getPadding(header.biWidth), out);
    }
    return WRITE_OK;

}
